# Rozwiazanie rownania postaci Au=g Agnieszka Bylica
import numpy

def LU (A,D,C):
  "Funkcja przeprowadza rozklad LU macierzy trojdiagonalnej"
  for i in range(len(D)-1):
    A[i] = A[i]/D[i] #Li
    D[i+1]=D[i+1] - A[i]*C[i] #Ui

def SolveA(A,D,C,G):
  "Funkcja rozwiazuje rownanie z macierza trojdiagonalna i zwraca wynik w postaci tablicy"
  N = len(D)

  LU(A,D,C)
  
  # Forward substitution
  for i in range(1,N):
    G[i] = G[i] - A[i-1]*G[i-1]  # L*y = g => Y
  # Back substitution 
  G[len(G)-1] = G[len(G)-1] / D[len(D)-1]
  for i in range(N-2,-1,-1):
    G[i] = (G[i] - C[i]*G[i+1])/D[i] # U*u=y
  return G

N = 1001
a = -(N*N)+(2.0*N)-1.0
b = (2.0*N*N)-(4*N)+3.0
A = numpy.empty(N-1) #pod diagonala
A.fill(a)
A[len(A)-1] = 0.0
  
D = numpy.empty(N)  # diagonala
D.fill(b)
D[0] = 1.0
D[len(D)-1] = 1.0
  
C = numpy.empty(N-1) # nad diagonala
C.fill(a)
C[0] = 0.0

G = numpy.empty(N) # wyrazy wolne
G.fill(0.0)
G[0] = 1.0
G[len(G)-1] = 1.0

Wynik = SolveA(A,D,C,G)
for i in range(1,N+1):
  print "u" + str(i) + " = " + "{0:.6g}".format(Wynik[i-1])
    

