# Rozwiazanie ukladu rownan.Agnieszka Bylica
import numpy


N = 1001

a = -(N*N)+(2.0*N)-1.0
b = (2.0*N*N)-(4.0*N)+3.0

u = numpy.empty(N)
u.fill(0.0)
u[0] = 1.0
u[2] = round(((-4.0 * a) - (3.0 * b)) / -1999999.0,10) #u3
u[1] = round((3.0 + u[2])/4.0,10) #u2

for i in range(3,N):
    u[i] = round(( (-a*u[i-2]) - (b*u[i-1]) )/a,10)


for i in range(1,N+1):
   x = (i-1.0)/(N-1.0)
   print str(x) + "   " + "{0:.6g}".format(u[i-1])
