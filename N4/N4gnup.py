# Rozwiazanie rownania postaci Au=g, dla macierzy A prawie trojdiagonalnej. Agnieszka Bylica
import numpy

def LU (N):
  "Funkcja przeprowadza rozklad LU macierzy trojdiagonalnej i zwraca liste [L,U]"
  A = numpy.empty(N-1)
  A.fill(1.0)
  D = numpy.empty(N)
  D.fill(4.0)
  D[0] = 3
  D[len(D)-1] = 3
  for i in range(len(D)-1):
    A[i] = A[i]/D[i] #Li
    D[i+1]=D[i+1] - A[i]*1.0 #Ui
  return [A,D] # [L,U]

def Fk( k, N):
  "Funkcja zwraca wartosc Fk = F(xk)"
  if N <= 0:
    raise ValueError("Size of vector < 1")
  fk = 1.0/( (100.0*(k**2)/(N**2)) - (100.0*k/N) + 26.0 )
  return fk

def CountG(N):
  "Funkcja oblicza wartosci wyrazow wolnych rownania i zwraca jako tablice G"
  a = 3.0*(N**2)/2
  G = numpy.empty(N-1)
  for i in range(2,N+1):
    G[i-2] = (Fk(i-2, N) - 2*(Fk(i-1, N)) + Fk(i, N))*a
  return G

def Solve(N):
  "Funkcja rozwiazuje rownanie z macierza prawie trojdiagonalna przy pomocy algorytmu Shermana-Morrisona i zwraca wynik w postaci tablicy"
  L,U = LU(N)
  G = CountG(N)
  # Forward substitution dla Az=g
  for i in range(1,N-1):
    G[i] = G[i] - L[i]*G[i-1]  # L*y = g => y
  # Back substitution dla Az=g
  G[N-2] = G[N-2] / U[N-2]    # Xn = Yn/Un
  for i in range(N-3,-1,-1):
    G[i] = (G[i] - 1.0*G[i+1])/U[i] # U*z=y => z
  
  # Forward substitution dla Aq=k
  L[0] = 1 # Yo
  for i in range(1,N-2):
    L[i] = - L[i]*L[i-1] # Yi
  L[N-2] = 1 - L[i]*L[i-1] # Yn-1
  # Back substitution dla Aq=k
  L[N-2] = L[N-2] / U[N-2]    #Qn-2
  for i in range(N-3,-1,-1):
    L[i] = (L[i] - 1.0*L[i+1])/U[i] # q
    
  # G is z, L is q => stosujemy wzor Shermana-Morrisona
  
  vTz = G[0] + G[N-2]
  vTq = L[0] + L[N-2]
  u = G - ( (vTz/1+vTq)*L )
  
  return u
  
  
N = 1000
Wynik = Solve(N)
for i in range(1,N):
  print str(-1.0 + (i*2.0/N)) + "      " + "{0:.4g}".format(Wynik[i-1])
    

