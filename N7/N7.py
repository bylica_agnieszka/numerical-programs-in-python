# Implementacja metody gradientow sprzezonych dla macierzy trojdiagonalnej, symetrycznej. Agnieszka Bylica
import numpy

def GradS(A,D,C,b):
  """Metoda gradientow sprzezonych dla macierzy trojdiagonalnej. x0 = [0...0]"""
  
  # Warunki poczatkowe dla x0 = [000...00]
  x = numpy.empty(len(b))
  x.fill(0.0)
  r1 = b
  p = numpy.copy(b)
  epsilon = 0.000001
  alfa = 0.0
  beta = 0.0
  
  while (numpy.linalg.norm(r1) > epsilon):
    Apk = countApk(A,D,C,p)
    alfa = r1.dot(r1)/p.dot(Apk)
    r2 = numpy.copy(r1)
    r1 = r1 - (alfa*Apk)
    beta = r1.dot(r1)/r2.dot(r2)
    x = x + (alfa*p)
    p = r1 + (beta*p)
  return x
    
    
def countApk(A,D,C,v):
  """Mnozy macierz trojdiagonalna przez wektor """
  N = len(v)
  vector = numpy.empty(N)
  vector[0] = D[0]*v[0] + C[0]*v[1]
  for i in range(1,N-1):
    vector[i] = A[i-1]*v[i-1] + D[i]*v[i] + C[i]*v[i+1]
  vector[N-1] = A[N-2]*v[N-2] + D[N-1]*v[N-1]
  return vector
  

A1 = numpy.array([-1,-1])
D1 = numpy.array([4,4,4])
C1 = numpy.array([-1,-1])
v1 = numpy.array([1,0,1])
print "Wynik dla macierzy 3x3: " 
Wynik = GradS(A1,D1,C1,v1)
for i in range(0,3):
  print "u" + str(i) + " = " + "{0:.6g}".format(Wynik[i])

N = 4
a = -(N*N)+(2.0*N)-1.0
b = (2.0*N*N)-(4*N)+3.0

A = numpy.empty(N-1) #pod diagonala
A.fill(a)

D = numpy.empty(N)  # diagonala
D.fill(b)

C = numpy.empty(N-1) # nad diagonala
C.fill(a)

b = numpy.empty(N)
b.fill(0.0)
b[0] = -1*a
b[N-1] = -1*a

Wynik = GradS(A,D,C,b)
print "Wynik dla macierzy z zadania 5a dla N=999: "
for i in range(0,N):
  print "u" + str(i) + " = " + "{0:.6g}".format(Wynik[i])
