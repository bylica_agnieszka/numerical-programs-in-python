import math

def countXn( n , N ) :
	return (math.pi /N)*(n+0.5)

def countTk( k , Xn ) :
	if  k == 0:
		Tk = 1.0
	elif  k == 1:
		Tk = Xn
	else:
		Tk = math.cos( k * math.acos( Xn ) )
	return Tk

def countIk( k , N ) :
    Ik = 0.0
    for n in range(N) :
        Xn = countXn( n*1.0 , N*1.0 )
        #print "   " + str(countTk( k , math.cos(Xn) )) + "   /" + str(( 1.0 + 25.0 * ( ( math.cos(Xn) )**2) ))
        Ik += countTk( k , math.cos(Xn) ) / ( 1.0 + 25.0* ( ( math.cos(Xn) )**2) )
        #print "Ik dla n= " + str(n) + " : " + str(Ik)
    return Ik

for i in range(20):
  print str(i)+" = " + "{0:.10g}".format(countIk(i,20))


