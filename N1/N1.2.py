import math

def countXn( n , N ) :
	return (math.pi /N)*(n+0.5)

def countTk( k , Xn ) :
	if  k == 0.0:
		return 1.0
	elif  k == 1.0:
		return Xn
	else:
		return (2.0 * Xn * countTk(k-1.0 , Xn )) - countTk(k-2.0 , Xn )

def countIk( k , N ) :
    Ik = 0.0
    for n in range(N) :
        Xn = countXn( n*1.0 , N*1.0 )
        Ik += countTk( k , math.cos(Xn) ) / ( 1.0 + 25.0 * ( ( math.cos(Xn) )**2) )
    return Ik

for i in range(20):
  print str(i)+" = " + "{0:.10g}".format(countIk(i,20))


