# Obliczenia metod iteracyjnych dla macierzy z zadania N5, trojdiagonalnej symetrycznej
import numpy
from N6 import *

N = 4
a = -(N*N)+(2.0*N)-1.0
b = (2.0*N*N)-(4*N)+3.0

A = numpy.empty(N-1) #pod diagonala
A.fill(a)

D = numpy.empty(N)  # diagonala
D.fill(b)

C = numpy.empty(N-1) # nad diagonala
C.fill(a)

b = numpy.empty(N)
b.fill(0.0)
b[0] = -1*a
b[N-1] = -1*a

Wynik,licznik = relaksacyjna(A,D,C,b,0.08)
print "Wynik dla macierzy N5 metoda relaksacyjna przy liczbie operacji: " + str(licznik) + " i gamma= " + str(0.08)
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])

Wynik,licznik = Jacobie(A,D,C,b)  
print "Wynik dla macierzy N5 metoda Jacobiego przy liczbie operacji: " + str(licznik) 
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])

Wynik,licznik = GS(A,D,C,b)  
print "Wynik dla macierzy N5 metoda Gausa-Seidla przy liczbie operacji: " + str(licznik) 
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
Wynik,licznik,omega = SOR(A,D,C,b)
print "Wynik dla macierzy N5 metoda SOR przy liczbie operacji: " + str(licznik) + " dla omega = " + str(omega)
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
N = 10
a = -(N*N)+(2.0*N)-1.0
b = (2.0*N*N)-(4*N)+3.0

A = numpy.empty(N-1) #pod diagonala
A.fill(a)

D = numpy.empty(N)  # diagonala
D.fill(b)

C = numpy.empty(N-1) # nad diagonala
C.fill(a)

b = numpy.empty(N)
b.fill(0.0)
b[0] = -1*a
b[N-1] = -1*a

"""Wynik,licznik = relaksacyjna(A,D,C,b,0.08)
print "Wynik dla macierzy N5 metoda relaksacyjna przy liczbie operacji: " + str(licznik) + " i gamma= " + str(0.25)
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.6g}".format(Wynik[i])"""

Wynik,licznik = Jacobie(A,D,C,b)  
print "Wynik dla macierzy N5 metoda Jacobiego przy liczbie operacji: " + str(licznik) 
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])

Wynik,licznik = GS(A,D,C,b)  
print "Wynik dla macierzy N5 metoda Gausa-Seidla przy liczbie operacji: " + str(licznik) 
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
Wynik,licznik,omega = SOR(A,D,C,b)
print "Wynik dla macierzy N5 metoda SOR przy liczbie operacji: " + str(licznik) + " dla omega = " + str(omega)
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
N = 100
a = -(N*N)+(2.0*N)-1.0
b = (2.0*N*N)-(4*N)+3.0

A = numpy.empty(N-1) #pod diagonala
A.fill(a)

D = numpy.empty(N)  # diagonala
D.fill(b)

C = numpy.empty(N-1) # nad diagonala
C.fill(a)

b = numpy.empty(N)
b.fill(0.0)
b[0] = -1*a
b[N-1] = -1*a

"""Wynik,licznik = relaksacyjna(A,D,C,b,0.08)
print "Wynik dla macierzy N5 metoda relaksacyjna przy liczbie operacji: " + str(licznik) + " i gamma= " + str(0.25)
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.6g}".format(Wynik[i])"""

Wynik,licznik = Jacobie(A,D,C,b)  
print "Wynik dla macierzy N5 metoda Jacobiego przy liczbie operacji: " + str(licznik) 
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])

Wynik,licznik = GS(A,D,C,b)  
print "Wynik dla macierzy N5 metoda Gausa-Seidla przy liczbie operacji: " + str(licznik) 
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
Wynik,licznik,omega = SOR(A,D,C,b)
print "Wynik dla macierzy N5 metoda SOR przy liczbie operacji: " + str(licznik) + " dla omega = " + str(omega)
for i in range(0,len(b)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
 

