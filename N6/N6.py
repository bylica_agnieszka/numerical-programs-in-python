# Implementacja metod iteracyjnych rozwiazywania ukladow rownan liniowych. Agnieszka Bylica
import numpy
import math

def countAv(A,D,C,v):
    """Mnozy macierz trojdiagonalna przez wektor """
    N = len(v)
    vector = numpy.empty(N)
    if D != None:
        vector[0] = D[0]*v[0] + C[0]*v[1]
        for i in range(1,N-1):
            vector[i] = A[i-1]*v[i-1] + D[i]*v[i] + C[i]*v[i+1]
        vector[N-1] = A[N-2]*v[N-2] + D[N-1]*v[N-1]
    elif D == None:
        vector[0] = C[0]*v[1]
        for i in range(1,N-1):
            vector[i] = A[i-1]*v[i-1] + C[i]*v[i+1]
        vector[N-1] = A[N-2]*v[N-2]
    return vector
  
def relaksacyjna(A,D,C,b,gamma):
    """Metoda relaksacyjna"""
    x1 = numpy.empty(len(b))
    x1.fill(0.0)
    x2 = numpy.copy(x1)
    dokladnosc = 10**-10
    licznik = 0.0
    k = 0.0
    while True:
        x2 = x1 + gamma*(b - countAv(A,D,C,x1))
        licznik = licznik + 1.0
        k = numpy.linalg.norm(x2-x1)
        if (k <= dokladnosc) or (licznik > 40.0):
            break
        else:
            x1 = numpy.copy(x2)
    return (x2,licznik)

def Jacobie(A,D,C,b):
    """Metoda Jacobiego dla macierzy trojdiagonalnej symetrycznej"""
    N = len(b)
    x1 = numpy.empty(N)
    x1.fill(0.0)
    x2 = numpy.copy(x1)
    dokladnosc = 10**-10
    licznik = 0.0
    while True:
        x2[0] = (b[0]-C[0]*x1[1])/D[0]
        for i in range(1,N-1):
            x2[i] = ( b[i] - (A[i-1]*x1[i-1]) - (C[i]*x1[i+1]) )/D[i]
        x2[N-1] = (b[N-1]-A[N-2]*x1[N-2])/D[N-1]
        licznik = licznik + 1.0
        if (numpy.linalg.norm(x2-x1)) <= dokladnosc:
            break
        else:
            x1 = numpy.copy(x2)
    return (x2,licznik)

def GS(A,D,C,b):
    """Metoda Gausa-Seidla dla macierzy trojdiagonalnej"""
    N = len(b)
    x1 = numpy.empty(N)
    x1.fill(0.0)
    x2 = numpy.copy(x1)
    y = numpy.empty(N)
    dokladnosc = 10**-10
    licznik = 0.0
    while True:
        x2[0] = (b[0]-C[0]*x1[1])/D[0]
        for i in range(1,N-1):
            x2[i] = ( b[i] - (A[i-1]*x2[i-1]) - (C[i]*x1[i+1]) )/D[i]
        x2[N-1] = (b[N-1]-A[N-2]*x2[N-2])/D[N-1]
        licznik = licznik + 1.0
        if (numpy.linalg.norm(x2-x1)) <= dokladnosc:
            break
        else:
            x1 = numpy.copy(x2)
    return (x2,licznik)

def SOR(A,D,C,b):
    """Metoda Successive OverRelaxation dla macierzy trojdiagonalnej"""
    omega = 1.1
    N = len(b)
    x1 = numpy.empty(N)
    x1.fill(0.0)
    x2 = numpy.copy(x1)
    dokladnosc = 10**-10
    licznik = 0.0
    while True:
        x2[0] = (1.0-omega)*x1[0] + (omega/D[0])*(b[0]-C[0]*x1[1])
        for i in range(1,N-1):
            x2[i] = (1.0-omega)*x1[i] + (omega/D[i])*( b[i] - (A[i-1]*x2[i-1]) - (C[i]*x1[i+1]) )
        x2[N-1] = (1.0-omega)*x1[N-1] + (omega/D[N-1])*(b[N-1]-A[N-2]*x2[N-2])
        licznik = licznik + 1.0
        if (numpy.linalg.norm(x2-x1)) <= dokladnosc:
            break
        else:
            x1 = numpy.copy(x2)
    return (x2,licznik,omega)
        
    
