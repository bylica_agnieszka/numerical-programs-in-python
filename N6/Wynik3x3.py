# Obliczenia metod iteracyjnych z N6 dla macierzy 3x3
import numpy
from N6 import *

A1 = numpy.array([-1,-1])
D1 = numpy.array([4,4,4])
C1 = numpy.array([-1,-1])
v1 = numpy.array([1,0,1])

Wynik,licznik = relaksacyjna(A1,D1,C1,v1,0.25)
print "Wynik dla macierzy 3x3 metoda relaksacyjna przy liczbie operacji: " + str(licznik) + " i gamma= " + str(0.25)
for i in range(0,len(D1)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
Wynik,licznik = Jacobie(A1,D1,C1,v1)
print "Wynik dla macierzy 3x3 metoda Jacobiego przy liczbie operacji: " + str(licznik) 
for i in range(0,len(D1)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
Wynik,licznik = GS(A1,D1,C1,v1) 
print "Wynik dla macierzy 3x3 metoda Gausa-Seidla przy liczbie operacji: " + str(licznik) 
for i in range(0,len(D1)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])
  
Wynik,licznik,omega = SOR(A1,D1,C1,v1) 
print "Wynik dla macierzy 3x3 metoda SOR przy liczbie operacji: " + str(licznik) + " dla omega = " + str(omega)
for i in range(0,len(D1)):
  print "u" + str(i) + " = " + "{0:.10g}".format(Wynik[i])