import math

def limes( x, i ):
    return ((math.exp(x+i) - math.exp(x)) / i )
  

i = 0.0000010
while i > 0.0:
    print "{0:.9g}".format(limes(1,i)) + "	h=" + str(i)
    i -= 0.0000001



