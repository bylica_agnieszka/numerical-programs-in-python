# Poszukiwanie wartosci wlasnych macierzy symetrycznej metoda potegowa.Agnieszka Bylica
import numpy
import scipy.linalg


def potega(A):
    """Metoda potegowa dla macierzy A"""
    yk = numpy.array([1.0,0.0,0.0]) #norma = 1
    zk = numpy.empty(3)
    epsilon = 10**-8
    znak = 1.0
    while True:
        zk = A.dot(yk)
        yk2 = zk/numpy.linalg.norm(zk)
        if( numpy.linalg.norm(yk2-yk) ) <= epsilon:
            break
        elif numpy.linalg.norm(yk2 + yk) == 0.0:
            znak = -1.0
            break
        else:
            yk = numpy.copy(yk2)
    lambda1 = numpy.linalg.norm(zk) * znak # wartosc wlasna 1
    
    e1 = yk2 # ostatnie (najblizsze) przyblizenie wektora wlasnego lambda1
    yk = numpy.cross(numpy.array([1.0,0.0,0.0]),e1) #wektor ortogonalny do e1
    znak = 1.0
    while True:
        zk = A.dot(yk)
        zk = zk - e1*(e1.dot(zk))
        yk2 = zk/numpy.linalg.norm(zk)
        if ( numpy.linalg.norm(yk2-yk) ) <= epsilon:
            break
        elif numpy.linalg.norm(yk2 + yk) == 0.0:
            znak = -1.0
            break
        else:
            yk = numpy.copy(yk2)

    lambda2 = numpy.linalg.norm(zk) * znak
    
    e2 = yk2 # ostatnie (najblizsze) przyblizenie wektora wlasnego lambda2
    yk = numpy.cross(e1,e2) # wektor ortogonalny do poprzednich wektorow wlasnych   
    znak = 1.0
    while True:
        zk = A.dot(yk)
        zk = zk - e2*(e2.dot(zk))
        yk2 = zk/numpy.linalg.norm(zk)
        if ( numpy.linalg.norm(yk2-yk) ) <= epsilon:
            break
        elif numpy.linalg.norm(yk2 + yk) == 0.0:
            znak = -1.0
            break
        else:
            yk = numpy.copy(yk2)

    lambda3 = numpy.linalg.norm(zk) * znak
    
    return (lambda1,lambda2,lambda3)
    
    
    
A = numpy.array([ [1.0,2.0,3.0], [2.0,4.0,5.0], [3.0,5.0,-1.0] ] )
print A 
print "Wartosci wlasne dla macierzy A wynosza: "
for i in potega(A):
     print "{0:.8g}".format(i)