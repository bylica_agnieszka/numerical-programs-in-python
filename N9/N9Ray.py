# Poszukiwanie wartosci wlasnych macierzy symetrycznej metoda Rayleigha. Agnieszka Bylica
import numpy

def rayleigh(A,vk):
    """Metoda Rayleigha dla macierzy A"""
    array1 = numpy.array([ [1.0,0.0,0.0], [0.0,1.0,0.0], [0.0,0.0,1.0] ] ) #macierz jednostkowa
    lambda_k = (A.dot(vk)).dot(vk)
    vk2 = numpy.copy(vk)
    
    while True:
        w = numpy.linalg.solve(A-(lambda_k*array1),vk) #lambda k
        vk = w/numpy.linalg.norm(w)
        lambda_k = (A.dot(vk)).dot(vk) # lambda k+1
        if numpy.linalg.norm(vk + vk2) == 0.0:
            break
        elif numpy.linalg.norm(vk - vk2) == 0.0:
            break
        else:
            vk2 = numpy.copy(vk)
            
    return (lambda_k,vk)
    

A = numpy.array([ [1.0,2.0,3.0], [2.0,4.0,5.0], [3.0,5.0,-1.0] ] )
print A
print "Wartosci wlasne dla macierzy A wynosza:"
vk = numpy.array([0.96,0.2,0.2])
lambda1,e1 = rayleigh(A,vk)
print "{0:.8g}".format(lambda1)

vk = numpy.cross(vk,e1)
lambda2, e2 = rayleigh(A,vk) 
print "{0:.8g}".format(lambda2)

vk = numpy.cross(e1,e2)
lambda3, e3 = rayleigh(A,vk) 
print "{0:.8g}".format(lambda3)