# Poszukiwanie wartosci wlasnych macierzy symetrycznej metoda QR. Agnieszka Bylica
import numpy
import math

def QR(A):
    """Metoda QR"""
    N = len(A)
    Qk,Rk = numpy.linalg.qr(A)
    W = []
    while True:
        Ak = Rk.dot(Qk)
        Qk,Rk = numpy.linalg.qr(Ak)
        if (round(Ak[N-1][0],8) == 0.0) and (round(Ak[N-1][1],8) == 0.0) and (round(Ak[N-2][0],8) == 0.0):
            break
    for i in range(N):
        W.append(Ak[i][i])
    return W







A = numpy.array([ [1.0,2.0,3.0], [2.0,4.0,5.0], [3.0,5.0,-1.0] ] )
print A
print "Wartosci wlasne dla macierzy A wynosza:"
for i in QR(A):
    print "{0:.8g}".format(i)